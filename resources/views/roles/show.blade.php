@extends('layouts.index')

@section('content')
     <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Show Role</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
              <li class="breadcrumb-item active">Show Role </li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid" style="width: 100%; margin: 0 auto;">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">
                    Role Detail
                </h3>
              </div>
              <div class="card-body">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                        <th>Name</th>
                        <th>Permissions</th>
                    </tr>
                  </thead>
                  <tbody>
                        <tr>
                            <td>{{ $role->name }}</td>
                            <td>
                            @if(!empty($rolePermissions))
                                @foreach($rolePermissions as $v)
                                    <label class="label label-success">{{ $v->name }},</label>
                                @endforeach
                            @endif
                            </td>
                        </tr>
                        <tr>
                        
                        </tr>
                        
                  </tbody>
                </table>
              </div>

              <div class="card-body">
                <table class="table table-bordered">
                  <tbody>
                        <tr>
                            <td>--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</td>
                        </tr>
                  </tbody>
                </table>
              </div>


            </div>
            <!-- /.card -->

          </div>
        </div>
        <!-- /.row -->

      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
