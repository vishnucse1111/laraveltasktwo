@extends('layouts.index')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Product</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
              <li class="breadcrumb-item active">Edit Product</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Product</h3>
              </div>
              <!-- /.card-header -->

              @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                    </ul>
                </div>
            @endif
              <!-- form start -->
              {!! Form::model($product, ['method' => 'PATCH','route' => ['products.update', $product->id]]) !!}
            
                <div class="card-body">
                  <div class="form-group">
                    <label for="product_name">Product Name</label>
                    {!! Form::text('product_name', null, array('placeholder' => 'Product Name','class' => 'form-control')) !!}
                  </div>

                  <div class="form-group">
                    <label for="description">Product Detail</label>
                    {!! Form::textarea('description', null, array('placeholder' => 'Product description','class' => 'form-control')) !!}
                  </div>

                  <div class="form-group">
                    <label for="image">Product Image</label>
                    <br>
                    <input type="file" class="form-control" style="height:50px" name="image" placeholder="Select Product Image">
  
                  </div>

                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

    <div class="card-body">
      <table class="table table-bordered">
        <tbody>
              <tr>
                  <td>--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</td>
              </tr>
        </tbody>
      </table>
    </div>

  </div>
  <!-- /.content-wrapper -->

  @endsection
  