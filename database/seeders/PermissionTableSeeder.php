<?php
  
namespace Database\Seeders;
  
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
  
class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'view-product',
            'add-product', 
            'update-product',
            'delete-product',

            'view-user',
            'add-user',
            'update-user', 
            'delete-user',

            'view-role',
            'add-role',
            'update-role', 
            'delete-role',
        ];
     
        foreach ($permissions as $permission) {
             Permission::create(['name' => $permission]);
        }
    }
}